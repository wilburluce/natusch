import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectMinerComponent } from './components/project-miner/project-miner.component';
import { TbdComponent } from './components/tbd/tbd.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectMinerComponent
  },
  {
      path: 'table',
      component: ProjectMinerComponent
  },
  {
     path: 'tbd',
     component: TbdComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false, enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
