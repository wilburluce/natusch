export interface ProjectItem {
  object_id: string;
  record_id: string;
  date_opened: Date | string;
  record_status: string;
  date_closed: string;
  full: string;
  record_type: string;
  record_type_category: string;
  record_type_group: string;
  record_type_subtype: string;
  record_type_type: string;
  record_type_4level: string;
  address: string;
  description: string;
  planner_id: string;
  module: string;
  city: string;
  state: string;
  zip: number;
  relevancy?: number;
  residential?: boolean;
  mixed_use?: boolean;
  commercial?: boolean;
  total_sq_ft: number;
}
