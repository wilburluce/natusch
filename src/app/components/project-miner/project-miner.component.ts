import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ProjectItem } from '../../project-item.model';
import { projectList } from '../../data/filtered';
import { ProjectItemProcessor } from './project-item-processor';

@Component({
  selector: 'app-project-miner',
  templateUrl: './project-miner.component.html',
  styleUrls: ['./project-miner.component.scss']
})
export class ProjectMinerComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<ProjectItem>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'record_id', 'relevancy', 'total_sq_ft', 'commercial', 'mixed_use', 'residential', 'date_opened', 'record_status', 'description'];

  /**
   'date_closed', 'Full', 'record_type', 'record_type_category',
   'record_type_group', 'record_type_subtype', 'record_type_type', 'record_type_4level', 'Address',
   'description', 'planner_id', 'module', 'city', 'state', 'zip'
   **/

  ngOnInit() {
    this.dataSource = new MatTableDataSource<ProjectItem>(
      this.processProjectData(projectList));
    this.dataSource.connect();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  processProjectData(data: any) {
    data.forEach(row => {
      new ProjectItemProcessor(row).process();
    });
    return data;
  }

}

