import { ProjectItem } from '../../project-item.model';

export class ProjectItemProcessor {
  constructor(public row: ProjectItem) {}

  process() {
    this.deserialize();
    this.parseDescription();
  }

  deserialize(): void {
    let date = Date.parse(<string>this.row.date_opened);
    if (isNaN(date)) {
      this.row.date_opened = null;
    } else {
      this.row.date_opened = new Date(date);
    }
  }

  parseDescription(): void {
    const lexemes = this.row.description.split(/[ /]/);
    let lastNum = 0;
    let total = 0;
    let residential = false;
    let commercial = false;
    let mixed_use = false;
    lexemes.forEach((lex, i) => {
      switch (lex.toLowerCase()) {
        case 'sf':
        case 'gsf':
        case 'square':
        case 'sq':
        case 'sq.':
          total += lastNum || 0;
          lastNum = 0;
          break;
        case 'residential':
          residential = true;
          break;
        case 'mixed-use':
        case 'mixed':
          mixed_use = true;
          break;
        case 'commercial':
          commercial = true;
          break;
        default:
          lastNum = this.parseNumber(lex);
      }
    });
    this.row.commercial = commercial;
    this.row.residential = residential;
    this.row.total_sq_ft = total;
    this.row.mixed_use = mixed_use;
    this.row.relevancy = this.calcRelevancyScore();
  }

  calcRelevancyScore(): number {
    let score = 0;
    if (this.row.commercial) {
      score += 100;
    }
    if (!this.row.commercial && this.row.mixed_use) {
      score += 50;
    }
    if (this.row.residential) {
      score -= 100;
    }

    score += this.row.total_sq_ft / 2000;

    return Math.round(score);
  }

  parseNumber(lex: string): number {
    const num = parseInt(lex.replace(',', ''), 10);
    return isNaN(num) ? 0 : num;
  }
}
